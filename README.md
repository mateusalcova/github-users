# Github Search User

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

This is my first project using Angular! 🎉  

A little about: Type the username in the search bar and the project will returns the user with some infos and your repositories.


## Getting Started

### Prerequisites

* [Node v8.11.3+](https://nodejs.org/en/);
* [NPM v6.4.1+](https://www.npmjs.com/get-npm);
* [Angular CLI v7.0.4.](https://github.com/angular/angular-cli) if you want to run commands of the CLI inside the project.

### Installing

Run the command:

```
npm install
npm start
```

**npm start**: for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Code scaffolding

**Prerequisites**: Angular CLI.

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

To get more help on the Angular CLI use `ng help`.


## Build

Run `ng build` or `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).



## Built With

* [Angular Material](https://material.angular.io/) - for the Visual/Layout, theme: **deeppurple-amber**
* [Material Icons](https://material.io/tools/icons/) - Icons
