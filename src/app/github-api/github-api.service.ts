import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GithubApiService {

  private readonly API = 'https://api.github.com';

  constructor() { }

  async getUser(username) {
    const response = await fetch(`${this.API}/users/${username}`);
    const data = await response.json();
    return data;
  }

  async getRepositories(username) {
    const response = await fetch(`${this.API}/users/${username}/repos`);
    const data = await response.json();
    return data;
  }

}
