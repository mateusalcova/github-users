import { HeaderComponent } from './header.component';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MaterialModule } from '../material/material.module';


describe('HeaderComponent', () => {

  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      declarations: [
        HeaderComponent,
      ]
    }).compileComponents();
  }));

  it('Should create', () => {

    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    });

    const fixture = TestBed.createComponent(HeaderComponent);
    const component = fixture.componentInstance;
    expect(component).toBeDefined();

  });


  it('Should output the title: Github Search Users', () => {

    fixture = TestBed.createComponent(HeaderComponent);
    let result = fixture.nativeElement.querySelector('h1');
    expect(result.innerText).toEqual('Github Search Users');

  });


});
