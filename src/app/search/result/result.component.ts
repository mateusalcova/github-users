import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.styl'],
})

export class ResultComponent implements OnInit {

  isResultLoaded: (boolean) = false;

  @Input() userInfos: (object) = {};
  @Input() userRepositories: (any);

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if( !Object.keys(this.userInfos).length )  return false;
    this.isResultLoaded = true;
  }

  closeResult() {
    location.reload();
  }

}
