import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-infos',
  templateUrl: './user-infos.component.html',
  styleUrls: ['./user-infos.component.styl']
})
export class UserInfosComponent implements OnInit {

  @Input() data: (any);

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if( !this.data ) return false;
    this.data = [this.data];
  }


}
