import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-repositories',
  templateUrl: './user-repositories.component.html',
  styleUrls: ['./user-repositories.component.styl']
})
export class UserRepositoriesComponent implements OnInit {

  @Input() data: (any);

  constructor() { }

  ngOnInit() {
  }

}
