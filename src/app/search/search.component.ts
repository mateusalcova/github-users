import { Component, OnInit } from '@angular/core';

// Services
import { GithubApiService } from './../github-api/github-api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.styl'],
})


export class SearchComponent implements OnInit {

  userInfos: (any) = {};
  userRepositories: (any);

  username: (any) = null;

  searching: (boolean) = false;
  animateSearch: (boolean) = false;

  constructor(public GithubApiService: GithubApiService) { }

  searchUser() {
    if( !this.username ) return this.addError();
    if( this.searching ) return false;

    this.searching = true;
    setTimeout(() => this.getUser(), 100)

  }

  ngOnInit() {
  }

  setValueInUsername(event) {
    this.username = event.target.value;
  }

  removeError() {
    if( !this.username ) this.username = null;
  }

  addError() {
    this.username = '';
  }

  resetForm() {
    this.username = null;
    this.searching = false;
  }


  async getUser() {

    const data = this.GithubApiService.getUser(this.username);

    await data
      .then( async infos => {

        if( !infos.hasOwnProperty('name') ) {
          this.resetForm();
          return this.notFoundUser();
        }

        await this.getRepositories();

        this.animateSearchBox();
        this.userInfos = infos;

        this.resetForm();

      })
      .catch( () => this.notFoundUser() )

  }

  async getRepositories() {
    const data = this.GithubApiService.getRepositories(this.username);

    await data.then( resp => {
      this.userRepositories = resp;
    });
  }

  animateSearchBox() {
    this.animateSearch = true;
  }

  notFoundUser() {
    return alert("Sorry, I don't find any user with this username. :/")
  }

}
