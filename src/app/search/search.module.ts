// Modules
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { ResultComponent } from './result/result.component';
import { SearchComponent } from './search.component';
import { MaterialModule } from '../material/material.module';
import { UserInfosComponent } from './result/user-infos/user-infos.component';
import { UserRepositoriesComponent } from './result/user-repositories/user-repositories.component';


@NgModule({
  declarations: [
    SearchComponent,
    ResultComponent,
    UserInfosComponent,
    UserRepositoriesComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
  ],
  exports: [
    SearchComponent,
    ResultComponent
  ]
})

export class SearchModule {
}
